# PageBuider
## 介绍
PageBuilder可以借助AI生成Html页面
- 文转页: 根据需求文字，分析客户需求并生成页面
- 图转页：将图片转换为页面。
它使用 GPT-4 Vision 生成代码
## 效果展示

![demo](demo.mp4.gif)

## 使用
### 在线体验

[http://image2code.itbuilder.cn:7008/text2page](http://image2code.itbuilder.cn:7008/text2page)

### 本地运行
```shell
mvn install
java -jar target/page-builder-1.0.0.jar
```
访问
```
http://localhost:8080/text2page
```
### 设置openai api key

![setting.png](setting.png)

点击设置，设置openai api key
 **apk key需要可以访问gtp4的权限** 

### 上传图片
![upload.png](upload.png)
上传图片后gpt的Vision会识别图像并生成代码